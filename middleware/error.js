


class ErrorHandler extends Error {
    constructor(statusCode, message,token) {
      super(); 
      this.message = message;
      this.statusCode = statusCode;
      this.token = token;
    }
  }
  
  const handleError = (err, res) => {
    const { message,token } = err;
     let statusCode = err.status || 401;
     res.status(statusCode).json({
      status: "0",
      message,
      //statusCode
      token
      
    });
  };
  
  
  module.exports = {
    ErrorHandler,
    handleError
    //validFieldsRegister
  }