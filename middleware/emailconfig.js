const nodemailer = require('nodemailer');

// async..await is not allowed in global scope, must use a wrapper
const sendermail= async function (emailto,subject,text) {
    try {
  // Generate test SMTP service account from ethereal.email
  // Only needed if you don't have a real mail account for testing
  let testAccount = await nodemailer.createTestAccount();

  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth: {
        user: 'samolito92@gmail.com',
        pass: 'S@molito1992'
    }
  });

  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"Jean Samuel " <samolito92@gmail.com>', // sender address
    to: emailto, // list of receivers
    subject: subject, // Subject line
    text: text, // plain text body
    //html: "<b>Hello world?</b>" // html body
  });
 // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
  console.log("Message sent: %s", info.messageId);
 
  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  return "sent"
} catch (error) {
    console.log(error)
   }
};
module.exports={sendermail}