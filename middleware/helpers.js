const validator = require('validator')
const User = require('../model/user');
let  { handleError, ErrorHandler}  = require('../middleware/error');

const validFieldsRegister = (req, res, next) => {
    // let errors = [];
      if (req.body) {
          if(validator.isEmpty(req.body.username)){
           throw new ErrorHandler(401,'Missing username field',null);
          }
          if(validator.isEmpty(req.body.email)){
           throw new ErrorHandler(401,'Missing email field',null);
          }
          if(validator.isEmpty(req.body.password)){
           throw new ErrorHandler(401,'Missing password field',null);
          }
          if(validator.isEmpty(req.body.confirmPassword)){
           throw new ErrorHandler(401,'Missing confirm Password field',null);
          }
          if(!validator.isEmail(req.body.email)){
           throw new ErrorHandler(401,'Incorrect email',null);
          }
            if (req.body.password !== req.body.confirmPassword) {
            throw new ErrorHandler(401,'Passwords do not match',null);
          } 
           else {
            return next();
        }
            
        }else{
            throw new ErrorHandler(500,'Server not found',null);
        }
  };

 const validFieldslogin = (req, res, next) => {
    // let errors = [];
    if (req.body) {
        const {email, password } = req.body;
        if(validator.isEmpty(req.body.email)){
         throw new ErrorHandler(401,'Missing email field',null);
        }
        if(validator.isEmpty(req.body.password)){
         throw new ErrorHandler(401,'Missing password field',null);
        }
        if(!validator.isEmail(req.body.email)){
         throw new ErrorHandler(401,'Incorrect email',null);
        }  

         else {
          return next();
      }
          
      }else{
          throw new ErrorHandler(500,'Server not found',null);
      }
      
  };
  const validFieldschangepassword = (req, res, next) => {
    // let errors = [];
    if (req.body) {
        if(validator.isEmpty(req.body.email)){
            throw new ErrorHandler(401,'Missing email field',null);
           }
           if(validator.isEmpty(req.body.currentPassword)){
            throw new ErrorHandler(401,'Missing current password field',null);
           }
           if(validator.isEmpty(req.body.newPassword)){
            throw new ErrorHandler(401,'Missing new password field',null);
           }
           if(validator.isEmpty(req.body.confirmPassword)){
            throw new ErrorHandler(401,'Missing confirm Password field',null);
           }
           if(!validator.isEmail(req.body.email)){
            throw new ErrorHandler(401,'Incorrect email',null);
           }
             if (req.body.newPassword !== req.body.confirmPassword) {
             throw new ErrorHandler(401,'Passwords do not match',null);
           } 

         else {
          return next();
      }
          
      }else{
          throw new ErrorHandler(500,'Server not found',null);
      }
      
  };
  const validFieldsforgottenpassword = (req, res, next) => {
    // let errors = [];
    if (req.body) {
        if(validator.isEmpty(req.body.code)){
            throw new ErrorHandler(401,'Missing code field',null);
           } 
           if(validator.isEmpty(req.body.confirmPassword)){
            throw new ErrorHandler(401,'Missing confirm Password field',null);
           }
             if (req.body.newPassword !== req.body.confirmPassword) {
             throw new ErrorHandler(401,'Passwords do not match',null);
           } 
           if(validator.isEmpty(req.body.newPassword)){
            throw new ErrorHandler(401,'Missing new password field',null);
           }
         else {
          return next();
      }
          
      }else{
          throw new ErrorHandler(500,'Server not found',null);
      }
      
  };
  module.exports = {
    validFieldsRegister,
    validFieldslogin,
    validFieldschangepassword,
    validFieldsforgottenpassword
  }