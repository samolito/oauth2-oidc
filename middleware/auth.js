const jwt = require('jsonwebtoken'),
    secret = require('../config/env.config.js').jwt_secret;



const validJwt = function(req, res, next) {
    const token = req.header("token");
    if (!token) return res.status(401).json({ message: "Auth Error" });
  
    try {
      const decoded = jwt.verify(token, secret);
      req.user = decoded.user;
      next();
    } catch (e) {
      console.error(e);
      res.status(500).send({ message: "Invalid Token" });
    }
  };

 const permissionLevel = (permission_level) => {
    return (req, res, next) => {
        let user_permission_level = parseInt(req.user.permissionLevel);
       // let userId = req.jwt.userId;
        if (user_permission_level & permission_level) {
            return next();
        } else {
            return res.status(401).json({message:'you have no Authorization to perform this action'});
        }
    };
};
  module.exports={validJwt,permissionLevel}