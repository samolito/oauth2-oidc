
const validator = require('validator')
const bycrypt = require('bcryptjs');
const jwt = require ('jsonwebtoken');
jwtSecret = require('../config/env.config').jwt_secret
const User = require('../model/user');
let  { handleError, ErrorHandler}  = require('../middleware/error');
const emailconfig= require('../middleware/emailconfig');
var randomize = require('randomatic');
var DateDiff = require('date-diff');

//create user function
const create =  async (req, res, next) => {
  try {
  if (await User.findOne({ email: req.body.email })) { 
        throw new ErrorHandler(401,'User already exists',null);
    }
      const user = new User(req.body);
   
      //Generate Salt
        const salt = await bycrypt.genSalt(10);
        //Hash user password
        user.password = await bycrypt.hash(req.body.password,salt);
        user.confirmPassword = await bycrypt.hash(req.body.confirmPassword,salt)
        user.lastUpdated =Date.now();

    // save user
    await user.save();
    //Generate Token
    //const payload = { user: user };
    const payload = { user: { id: user.id, email:user.email,permissionLevel:user.permissionLevel} };
    let token = jwt.sign(payload,jwtSecret);
    res.status(201).json({ status:1, message: "User created successfully", token });
    next()
  } catch (error) {
    next(error)
  }
};


// login 
const login = async (req,res, next)=>{
  try {
  
    const {email, password } = req.body;
    const user = await User.findOne({email});
    if (!user)
    throw new ErrorHandler(401,'User Not Exist',null);
    //Verify Status User
    if(!user.status)
    throw new ErrorHandler(401,'your account has been deactivated due to lack of activity for more than 30 days.', null);

    // compare password 
    const isMatch = await bycrypt.compare(password, user.password);
    if (!isMatch)
    {
      throw new ErrorHandler(401,'Incorrect Password',null);
    }
    user.lastUpdated =Date.now();
    await user.save();
    //Generate token
    //const payload = { user: user };
    const payload = { user: { id: user.id, email:user.email,permissionLevel:user.permissionLevel} };
    let token = jwt.sign(payload,jwtSecret, {expiresIn: 60*60});
    res.status(200).json({ status:1, message: "User logged successfully", token });
  
        next()
  } catch (error) {
    next(error)
  }
}



//return all user 
const getAll =  async (req, res, next)=>{
  try {
   users = await User.find().select('-hash');
  return res.json({
    status:1,
    results: users,
  });
} catch (error) {
    next(error);
}
};



///////////////////////////////////////////////////////////////////
//Retrieve LoggedIn user
const getuser= async(req,res,next)=>{
  try {
    const user = await User.findOne({email:req.params.email})
    if(user){
     res.status(201).json({status:1,user:user});
   }
  else{
    throw new ErrorHandler(401,'User Not Exist',null);
  }
  } catch (error) {
   next(error)
  }

};



////////////////////
//Delete user function
const deleteuser= async(req,res,next)=>{
  const user = await User.findOne({email:req.body.email});
  try { 
  if(user){
    user.status = false
    user.lastUpdated= Date.now();
    user.save();
   //return new handleError(200,'User removed');
    res.status(200).json({token:null,
      message:'User removed',
      status:1});
    }
  else{
    throw new ErrorHandler(401,'User not exits',null);
  }
    next()
    } catch(error) {  
      next(error)
  }
};


//Change user password function
const changepassword= async(req,res, next)=>{
  try {
  
  const{email, currentPassword,newPassword,confirmPassword}= req.body;
  const user = await User.findOne({email});
  if(!user){
    throw new ErrorHandler(401,'user not exits',null);
  }
  //compare actual password 
  const isMatch = await bycrypt.compare(currentPassword, user.password);
  if (!isMatch)
  throw new ErrorHandler(401,'Current Password Incorrect ',null);  
  else{
  //Generate Salt
  const salt = await bycrypt.genSalt(10);
  //Hash user password
  user.password = await bycrypt.hash(newPassword,salt);
  user.confirmPassword = await bycrypt.hash(confirmPassword,salt);
  user.lastUpdated=Date.now();
  }
    // save user
    await user.save();
    //Generate token
    //const payload = { user: user };
    const payload = { user: { id: user.id, email:user.email,permissionLevel:user.permissionLevel} };
    let token = jwt.sign(payload,jwtSecret);
    res.status(200).json({ status:1, message: "Password changed succesfully", token });
      next()
  } catch (error) {
    next(error)
  }

};

//Logout
const logout=async(req, res) =>{

 var sess = req.session;
    if(sess){
      req.session = null;
      res.status(200).json({
        status:1,
        message: "user logout successfully",
        token: null, 
      });
  }
  res.status(200).json({ status:1, message: "user logout successfully",
    token: null, 
  });
};

//First
let code; // code for verification
let fp_user;  //forgotten pasword user
let datecode,datenow,exp; // timing for code expiration
///////////////////////////////////////////////////////////////////
//Send email verifcation
const sendmail= async(req,res,next)=>{
try {
   if(!validator.isEmail(req.body.email)){
     throw new ErrorHandler(401,'Invalid email',null);
    }
   fp_user = await User.findOne({email:req.body.email})
  if(req.body.email){
    code=randomize('0', 6);
    datecode=Date.now();
    //Mail configuration
    let subject= "Email Verification"; // Subject line
    let msg= 'Confirmtion code : '+code;
    const transporter = emailconfig.sendermail(req.body.email,subject,msg);
    res.status(200).json({ status:1, message:"Verification code sent", code:code
  });
   }
else{
  throw new ErrorHandler(401,'User Not Exist',null);
}
 } catch (error) {
 next(error)
}
};
///////////////////////////////////////////////////////////////////
//Confirm code and save new password
const changeforgettenpassword= async(req,res, next)=>{
  try {
  datenow=Date.now();
  var diff =new DateDiff(datenow,datecode);
  let exp=parseInt(diff.seconds());
  
   if(exp>30)
   {
     throw new ErrorHandler(401,'Code Expired',null);
   }
   //compare code
  let receivedCode = parseInt(req.body.code);
  if(code!=receivedCode){
    throw new ErrorHandler(401,'Code Incorrect',60);
  }

  else{
  //Generate Salt
  const salt = await bycrypt.genSalt(10);
  //Hash user password
  fp_user.password = await bycrypt.hash(req.body.newPassword,salt);
  fp_user.confirmPassword = await bycrypt.hash(req.body.confirmPassword,salt)
  fp_user.lastUpdated=Date.now();
  }
    await fp_user.save();
   // Generate Token
   //const payload = { user: user };
   const payload = { user: { id: fp_user.id, email:fp_user.email,permissionLevel:fp_user.permissionLevel} };
   
   let token = jwt.sign(payload,jwtSecret);
   res.status(200).json({ status:1, message: "Password recovered succesfully", token });
   code=null; fp_user=null; exp=null;
   
       next()
   } catch (error) {
     next(error)
   }
  
};

//Very User Status 
const verifyuserstatus=  async (req, res, next)=>{
  try {
   users = await User.find().select('-hash');
   users.forEach(user => {
    var diff =new DateDiff(Date.now(),user.lastUpdated);
     if(diff.days()>30){
       user.status= false;
       user.save()
      }
   }); 
   console.log('Verify Status Done');
} catch (error) {
    next(error);
}
};

setInterval(verifyuserstatus, 1000*60*60*24) 

const getme= async (req, res,next ) => {
  try {
    // request.user is getting fetched from Middleware after token authentication
    //const user = await User.findById(req.user.id);
    res.json(req.user);
  } catch (e) {
    res.send({ message: "Error in Fetching user" });
  }
};

module.exports = {create, 
  getAll, 
  login,
  getuser, 
  deleteuser,
  changepassword,
  logout,
  sendmail,
  changeforgettenpassword,
  verifyuserstatus,
getme};
