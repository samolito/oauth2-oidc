 
const router =  require('express').Router();
const userController = require('../controller/user.controller');
const auth = require('../middleware/auth');
const helpers = require('../middleware/helpers');
const config = require('../config/env.config');
/**
 * @api {post} /api/register 
 * @apiDescription Register a new user
 * @apiVersion 1.0.0
 * @apiName Register
 * @apiGroup Register
 * @apipermission public
 * 
 * @apiParam {Sting} username  user's name
 * @apiParam {String} email    unique and valid email.
 * @apiParam {String} password user passowerd
 * @apiParam {String} confirmPassword confirm password user.
 *
 * @apiSuccess {Number} status 
 * @apiSuccess {String} message Success message.
 * @apiSuccess {String} token  Access Token's type.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": 1
 *        "message": "User created successfully",
 *        "token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiNWU1ZjJmZmM5YTI2ZDMzODRmNzdmMWZiIiwidXNlcm5hbWUiOiJIZXJvZGUgc29tIiwiZW1haWwiOiJIZXJvZGVAZ21haWwuY29tIn0sImlhdCI6MTU4MzI5NjUwOCwiZXhwIjoxNTgzMzA2NTA4fQ.ZUHjt8VXUJO-NIToqgTOXNHsGD3Y8Ej1Bxp7S342i5M",
 *   }
 *
 * @apiError (Bad Request 401)   ValidationError  Some parameters may contain invalid values
 * @apiError (Forbidden 403)     Forbidden        Only admins can create the data
 */
 router.post('/register',
 helpers.validFieldsRegister,
 userController.create);


 /**
 * @api {get} /api/allusers 
 * @apiDescription Get a list of users
 * @apiVersion 1.0.0
 * @apiName Listallusers
 * @apiGroup Auth
 * @apipermission admin
 * 
 * @apiParam {String} token  user's access token
 *
 * @apiSuccess {Object[]} users List of users
 *
 * @apiError (Unauthorized 401)  Unauthorized   you have no Authorization to perform this action
 */
router.get('/allusers', 
auth.validJwt,
auth.permissionLevel(config.permissionLevels.NORMAL_USER),
userController.getAll);

//login user route 
router.post('/login',
helpers.validFieldslogin,
userController.login);

//logout user router
router.get('/logout',userController.logout); 

//Delete user route
router.delete('/user/delete',
auth.validJwt,
auth.permissionLevel(config.permissionLevels.ADMIN),
userController.deleteuser);

//Change user password
router.put('/user/changepassword',
helpers.validFieldschangepassword,
auth.validJwt,
auth.permissionLevel(config.permissionLevels.NORMAL_USER),
userController.changepassword);

//retrieve  user by email
router.get('/getuser/:email',
auth.validJwt,
userController.getuser);

//Email Verification
router.post('/verifyemail',
userController.sendmail);

//Recover password
router.post('/recoverpassword',
helpers.validFieldsforgottenpassword,
userController.changeforgettenpassword);



//Get me
router.get('/me',auth.validJwt,
userController.getme);


module.exports = router;