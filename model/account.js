const mongoose = require('mongoose')


const AccountSchema = mongoose.Schema({
acn:{
    type:String,
    required: true
},
balance:{
    type:Number,
    required: true
},
currency:{
    type:String,
    required: true
},
lastUpdated: { type: Date},
user:{
    type:mongoose.Schema.Types.ObjectId,
    ref:'user'
}
});
AccountSchema.set('toJSON', { virtuals: true });

//Export model user with UserChema
module.exports = mongoose.model("account", AccountSchema);