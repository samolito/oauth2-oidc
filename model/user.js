const mongoose = require('mongoose')


const UserSchema = mongoose.Schema({
username: { type:String,        required: true },
email:    { type:String,        required: true },
password: { type:String,        required: true  },
confirmPassword:{ type:String,  required: true },
status:   {  type:Boolean,      required: true,default:true },
permissionLevel:    { type:Number, default:1 },
createdDate: { type: Date, default: Date.now },
lastUpdated: { type: Date} });
UserSchema.set('toJSON', { virtuals: true });

//Export model user with UserChema
module.exports = mongoose.model("user", UserSchema);