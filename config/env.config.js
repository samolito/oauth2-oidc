module.exports = {
    "port": 4040,
    "appEndpoint": "http://localhost:4040",
    "apiEndpoint": "http://localhost:40400",
    "jwt_secret": "Ifisecret1234",
    "jwt_expiration": 1000,
    "environment": "dev",
    "permissionLevels": {
        "NORMAL_USER": 1,
        //"PAID_USER": 4,
        "ADMIN": 2048
    }
};
