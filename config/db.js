const mogoose = require('mongoose')

const url = "mongodb://localhost:27017/dbauth";

const Mongoserver = async()=>{

    try {
        await mogoose.connect(url,
            {
                poolSize: 10, // Maintain up to 10 socket connections
                useUnifiedTopology:true,
                useNewUrlParser: true

            });
            console.log("Connected with DB")
    } catch (err) {
        console.log(err);
        throw err;
    }
};
module.exports = Mongoserver;