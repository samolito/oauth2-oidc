const express = require('express'),
     bodyParser = require('body-parser'),
     MongoServer = require('./config/db'),
     config = require('./config/env.config'),
     user_route = require("./routes/user.routes"),
      handleError = require('./middleware/error').handleError
     app = express(),
     session = require('express-session'),
     swaggerUi = require('swagger-ui-express'),
     path=require('path')
     swaggerDocument = require('./swagg.json'),
     hat = require('hat'),
     token = hat();
//Initiate mongo Server
MongoServer(); 

//Port
 PORT = process.env.PORT || 4040
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//app.use(cors());

 app.use('/server',function(req,res){
    res.send('server connected ');
 });     
//  app.use('/docapi',function(req,res){ 
//   res.send('/apidoc/index.html');
// });
app.get('/ap', function (req, res) {
  res.sendFile(path.join(__dirname+ '/apidoc/index.html'));
});
 app.use('/apidoc',swaggerUi.serve, swaggerUi.setup(swaggerDocument));
//Call method Signup user
app.use('/api',user_route);

app.get('/error', (req, res) => {
  throw new ErrorHandler(500, 'Internal server error');
})
//Handler Error
app.use((err, req, res, next) => {
    handleError(err, res);
  });

//Start Server
app.listen(config.port, (req,res)=>{
    console.log('Server start at Port '+config.port);
})

module.exports=app;