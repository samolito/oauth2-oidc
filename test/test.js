const app = require('../server');
//assert = require('assert'),
const chai = require('chai');
const assert = require('chai');
const expect = chai.expect;;
request = require('supertest');
//should = require('should');

//login test
describe("Authentication", () => {

    it("should respond with session token", function (done){
       request(app)
        .post("/api/login")
            .send({
                email: "cephas@gmail.com",
                password: "wordpass1111",
            })
          .set('Accept','application/json')
          .expect(200)
          .expect((res) => {
              assert.equal(res.body.message, 'User logged successfully');
             // assert.equal(res.body.token);
           
              });    
              done();

    });
});
   
//Register user test
describe("Register", () => {

   it("should respond with user is successfully registered", function (done) {

      request(app).post("/register")
          .send({
            username: "Hollynios",
            email: "hollyn@gmail.com",
            password: "mypass1234",
            confirmPassword: "mypass1234",
          });
          expect(201)
          expect((res) => {
          assert.equal(res.body.message, 'User created successfully');
          assert.equal(res.body.token);
          
        });
        done()

  });

 });

//Get all users test
 describe("AllUsers",()=>{
  it("Should get all users record",()=>{
    const res= request(app).get('/allusers')
      .send()
      expect(response => {
        assert.strictEqual(res.body.length > 0, true);
       });
      
  });
});

//Get user test
describe("Get user",()=>{
  it("Should get  user record",()=>{
    const res= request(app).get('/user')
      .send({
        email:"hollyn@gmail.com"
      });
      expect(response => {
        //assert.strictEqual(response.body instanceof Buffer, true);
        assert.strictEqual(res.body.email,'hollyn@gmail.com');
       });
      
  });
});


   
//Change password user test
describe("Change Password", () => {

  it("should respond Password successfully changed", function(done){
     request(app).put("/user/changepassword")
          .send({
            email: "samolito@gmail.com",
            currentPassword: "mypass1234",
            newPassword: "pswd1234",
            confirmPassword: "pswd1234",
          })
          .set('Accept','application/json')
          .expect(200)
          .expect((res) => {
          assert.equal(res.body.message, 'Password changed succesfully');
          assert.equal(res.body.token);
          
        });
        done()
  });

 });

//Delete user test
describe("Delete user",()=>{
  it("Should delete user", function (done){
   request(app).delete('/user/delete')
      .send({
        email:"woodna@gmail.com"
      })
      .expect(200)
      .expect(response => {
      assert.equal(response.body.message, 'User removed');
        //assert.strictEqual(res.body.email,'hollyn@gmail.com');
       });
      done()
  });
});

//Change password user test
describe("Recover Password", () => {

  it("should respond Password successfully recovered", function (done)  {

      request(app).put("/user/recoverpassword")
          .send({
            
            code: "123456",
            newPassword: "pswd1234",
            confirmPassword: "paswd1234",
          })
          .expect(200)
          .expect((res) => {
          assert.equal(res.body.message, 'Password recovered succesfully');
          assert.equal(res.body.token);
          
        });
        done()

  });

});
